<?
set_time_limit(600);
require_once $_SERVER["DOCUMENT_ROOT"].'/autoloader.php';


$autoloader = new Autoloader();
$autoloader->addNamespace('Application', $_SERVER["DOCUMENT_ROOT"] . '/classes');
$autoloader->addNamespace('Application\\Element', $_SERVER["DOCUMENT_ROOT"] . '/classes/Element');
$autoloader->addNamespace('Application\\Interfaces', $_SERVER["DOCUMENT_ROOT"] . '/classes/interfaces');
$autoloader->addNamespace('Application\\Api', $_SERVER["DOCUMENT_ROOT"] . '/api/v1');
$autoloader->register();

Application\Api\Router::router();

?>