<?php
namespace Application\Api;
use \Application\Main;

class Router
{	
	public static function router()
	{
		$request_uri = explode('?', $_SERVER['REQUEST_URI']);
		$routes = explode('/', $request_uri[0]);
		$authUsers = json_decode(
			file_get_contents(
				$_SERVER["DOCUMENT_ROOT"] . '/api/v1/data/authkey.json'
			),
			TRUE
		);

		if (!empty($_POST['authkey'])) {
			if (in_array($_POST['authkey'], $authUsers)) 
			{
				echo json_encode(array('success' => true));
					//тут будет основной роут
			} else {
				echo json_encode(array('success' => false));
			}
		} elseif (!empty($_POST['login']) && !empty($_POST['pass'])) {
				$authkey = hash(md5, $_POST['login']);
				$authUsers[] = $authkey;
				file_put_contents($_SERVER["DOCUMENT_ROOT"] . '/api/v1/data/authkey.json', $authUsers);
				echo json_encode(array('success' => true, 'authkey' => $authkey));
		} else {
			echo json_encode(array('success' => false));
		}
	}
}
