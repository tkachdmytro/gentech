<?
namespace Application;

abstract class AbstractPopulation
{
    protected static $instance;
    protected $populations = array();
    protected $maxPopulation = 10;

    abstract public static function getInstance();
    abstract public function allMutation();
    abstract public function generateBase();
    abstract public function generate();
    abstract public function selection();

    public function setMaxPopulation($maxPopulation)
    {
        $this->maxPopulation = $maxPopulation;

        return $this;
    }

    public function addIndividuals($individuals)
    {
        $this->populations[] = $individuals;

        return $this;
    }

    public function getMaxPopulation()
    {
        return $this->maxPopulation;
    }

    public function getPopulations()
    {
        return $this->populations;
    }

    public function clearPopulations()
    {
        $this->populations[] = array();

        return $this;
    }

    public function findCouple($inviter)
    {
        $withoutInviter = array();
        foreach ($this->populations as $individual) {
            if ($individual !== $inviter) {
                $withoutInviter[] = $individual;
            }
        }
        $randInt = rand(0, sizeof($withoutInviter)-1);

        return $withoutInviter[$randInt];
    }

    public function sort()
    {
        usort($this->populations, function($a, $b){
            $aEstimate = $a->getEstimate();
            $bEstimate = $b->getEstimate();
            if ($aEstimate == $bEstimate) {
                return 0;
            }
            return ($aEstimate > $bEstimate) ? -1 : 1;
        });

        return $this;
    }

    public function getInfo()
    {
        $this->sort();

        $arResults = array();
        foreach ($this->populations as $individual) {
            $arResults[] = array(
                "ESTIMATE" => $individual->getEstimate(),
                "CODE" => $individual->getCode()
            );
        }

        return $arResults;
    }

    public function getEstimates()
    {
        $this->sort();

        $arResults = array();
        foreach ($this->populations as $individual) {
            $arResults[] = $individual->getEstimate();
        }

        return $arResults;
    }

    public function getBestIndividual()
    {
        $this->sort();

        return reset($this->populations);
    }
}