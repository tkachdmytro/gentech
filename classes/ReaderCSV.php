<?
namespace Application;

use Application\Interfaces\IReader;

class ReaderCSV implements IReader
{
    public $terminator = "\n";
    public $separator = ";";
    public $enclosed = '"';
    public $escaped = "\\";
    public $mimeType = "text/csv";

    private $filename;
    protected $headerColumns = array();
    protected $rows = array();
    //outside
    protected $inputMarks = array();
    protected $outputMarks = array();

    public function setFilename($filename)
    {
        $this->filename = $filename . '.csv';

        return $this;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setInputMarks($inputMarks)
    {
        $this->inputMarks = $inputMarks;

        return $this;
    }

    public function getInputMarks()
    {
        return $this->inputMarks;
    }

    public function setOutputMarks($output)
    {
        $this->outputMarks = $outputMarks;

        return $this;
    }

    public function getOutputMarks()
    {
        return $this->outputMarks;
    }

    public function headerColumns($columns = false)
    {
        if ($columns)
        {
            //START: set headerColumns
            if ($this->colCheckMismatch($columns))
            {
                echo 'Unable to add header columns - row column mismatch!';
            }
            else
            {
                if (is_array($columns))
                {
                    foreach($columns as $column)
                    {
                        $this->headerColumns[] = $column;
                    }
                }
                else
                {
                    $this->headerColumns[0] = $columns;
                }
            }
            //END: set headerColumns
        }
        else
        {
            return $this->headerColumns;
        }
    }

    public function getHeaderColumns()
    {
        return $this->headerColumns;
    }

    private function colCheckMismatch($row)
    {
        if ($this->headerColumns)
        {
          if (count($this->headerColumns) != count($row)) return true;
        }
        elseif (!$this->headerColumns && $this->rows)
        {
          if (count($this->rows[0]) != count($row)) return true;
        }

        return false;
    }

    public function readCSV($file, $headers = false)
    {
        $row = 0;

        if (($handle = fopen($file, "r")) !== FALSE)
        {
            //loop through rows
            while (($data = fgetcsv($handle, 0, $this->separator, $this->enclosed, $this->escaped)) !== FALSE)
            {
                $num = count($data);

                //error check, make sure the number of columns is consistent
                if ($row == 0)
                {
                    $first_row_columns = $num;

                    //loop through columns
                    $headerRow = array();
                    foreach ($data as $key => $value) {
                        $headerRow[$key] = $value;
                        if (strpos($value, "OUTPUT_") !== false) {
                            //$this->outputMarks[] = str_replace("OUTPUT_", "", $value);
                            $this->outputMarks[] = $value;
                        }
                        if (strpos($value, "INPUT_") !== false) {
                            //$this->inputMarks[] = str_replace("INPUT_", "", $value);
                            $this->inputMarks[] = $value;
                        }
                    }
                    $this->headerColumns($headerRow);
                }
                else
                {
                    if ($num != $first_row_columns)
                    {
                        echo 'The number of columns in row '.$row.' does not match the number of columns in row 0';
                        fclose($handle);
                        return false;
                    }

                    //loop through columns
                    for ($c=0; $c < $num; $c++)
                    {
                        $this->rows[$row-1][$this->headerColumns[$c]] = $data[$c];
                    }
                }

                $row++;
            }
            fclose($handle);
        }
    }

    public function getRow($row)
    {
        return $this->rows[$row];
    }


    public function getRowCol($row, $col)
    {
        return $this->rows[$row][$col];
    }

    public function getHeaderIndex($header)
    {

        if ($this->headerColumns)
        {
            return array_search($header, $this->headerColumns);
        }

        return false;
    }

    public function getRowIndex($col, $data)
    {

        if ($col && $this->rows)
        {

            $matchingRows = array();
            foreach($this->rows as $row => $rowData)
            {
                foreach($rowData as $column => $value)
                {
                    if ($value == $data) $matchingRows[] = $row;
                }
            }
            return $matchingRows;
        }

        return false;
    }

    public function totalRows()
    {
        return count($this->rows);
    }

    public function totalCols()
    {
        if ($this->headerColumns)
        {
            return count($this->headerColumns);
        }
        elseif (!$this->headerColumns && $this->rows)
        {
            return count($this->rows[0]);
        }

        return 0;
    }

}