<?
namespace Application;

use Application\Main;
use Application\SchemaBuilder;

class Schema
{
    protected static $instance;
    protected static $elementsCount;
    protected static $elementsType;
    protected static $elementInputCount;
    protected static $elementOutputCount;
    protected static $elementsInputCount;
    protected static $elementsOutputCount;
    //protected static $staticInputMarks = array("0", "1");
    protected static $inputMarks = array();
    protected static $outputMarks = array();
    protected static $staticInputMarks = array();
    protected static $allOutputMarks = array();
    protected static $skipInputMarks = array();
    protected $schema = array();
    protected $allInputMarks = array();
    protected $baseInputMarks = array();

    //protected $results = array();

    public function __construct() {
        $schemaBuilder = Main::getSchemaBuilder();
        self::$elementsCount = $schemaBuilder->getElementsCount();
        self::$elementsType = $schemaBuilder->getElementsType();
        self::$elementInputCount = $schemaBuilder->getElementInputCount();
        self::$elementOutputCount = $schemaBuilder->getElementOutputCount();
        self::$elementsInputCount = $schemaBuilder->getElementsInputCount();
        self::$elementsOutputCount = $schemaBuilder->getElementsOutputCount();
        self::$inputMarks = $schemaBuilder->getInputMarks();
        self::$outputMarks = $schemaBuilder->getOutputMarks();
        self::$staticInputMarks = $schemaBuilder->getStaticInputMarks();
        self::$allOutputMarks = $schemaBuilder->getAllOutputMarks();
        self::$skipInputMarks = $schemaBuilder->getSkipInputMarks();
        $this->allInputMarks = $schemaBuilder->getAllInputMarks();
        $this->baseInputMarks = array_diff($this->allInputMarks, $this->getSkipInputsToElement());
    }

    public function __clone()
    {
        $cloneSchema = array();
        foreach ($this->schema as $obj) {
            $cloneSchema[] = clone $obj;
        }
        $this->schema = $cloneSchema;
    }

    public static function create()
    {
        return new static();
    }

    public function setSchema($schema)
    {
        $this->schema = array();

        foreach ($schema as $value) {
            $this->schema[] = ElementFactory::create(self::$elementsType)
                ->setInputMarks($value["INPUT"])
                ->setOutputMarks($value["OUTPUT"]); 
        }

        return $this;
    }

    public function getSchema()
    {
        return $this->schema;
    }

    public function getSchemaCode()
    {
        $schemaCode = array();
        foreach ($this->schema as $obj) {
            foreach ($obj->getInputMarks() as $value) {
                $schemaCode[] = $value;
            }
        }

        return $schemaCode;
    }

    public function getStringSchemaCode($separator = "|")
    {
        return implode($separator, $this->getSchemaCode());
    }

    public function getSchemaArray()
    {
        $schema = array();
        foreach ($this->schema as $obj) {
            $schema[] = array(
                "INPUT" => $obj->getInputMarks(),
                "OUTPUT" => $obj->getOutputMarks()
            );
        }

        return $schema;
    }

    public function getSchemaJSON()
    {
        return json_encode($this->getSchemaArray());
    }

    public function getFreeInputMarks()
    {
        return array_diff(self::$inputMarks, $this->getSchemaCode());
    }

    public function getFreeInputs()
    {
        $results = array_diff($this->allInputMarks, $this->getSchemaCode());
        if (empty($results))
        {
            //Дополнительно добавляем статику
            $results = self::$staticInputMarks;
        }
        sort($results);

        return $results;
    }

    public function getSkipInputsToElement($elementPosition = 0)
    {
        return self::$skipInputMarks[$elementPosition];
    }

    public function getFreeOutputs()
    {
        $results = array_diff(self::$allOutputMarks, $this->getSchemaCode());

        return $results;
    }

    public function generate()
    {
        $outCounter = 1;
        $schema = array();
        $allInput = self::$staticInputMarks;
        
        //$this->schema[] = ElementFactory::create(self::$elementsType)
        for ($i = 0; $i < self::$elementsCount; $i++) {
            for ($j = 0; $j < self::$elementInputCount; $j++) {
                $randInt = rand(0, sizeof($allInput)-1);
                $schema[$i]["INPUT"][] = $allInput[$randInt];
                $allInput = Main::array_skip($allInput, $randInt);
            }
            for ($k = 0; $k < self::$elementOutputCount; $k++) {
                $schema[$i]["OUTPUT"][] = "Out".$outCounter;
                $allInput[] = "Out".$outCounter;
                $outCounter++;
            }
        }
        $this->setSchema($schema);

        while (sizeof($this->getFreeInputMarks())) {
            $freeInputMarks = $this->getFreeInputMarks();

            foreach ($freeInputMarks as $inputMark) {
                $randInt = rand(0, sizeof($this->schema)-1);
                $randElement = $this->schema[$randInt];
                $randElement->setRandomInputMark($inputMark);
            }

        }
       
        return $this;
    }

    public function generateOld()
    {
        $outCounter = 1;
        $schema = array();
        $allInput = $this->baseInputMarks;
        for ($i = 0; $i < self::$elementsCount; $i++) {
            for ($j = 0; $j < self::$elementInputCount; $j++) {
                $randInt = rand(0, sizeof($allInput)-1);
                $schema[$i]["INPUT"][] = $allInput[$randInt];
                $allInput = Main::array_skip($allInput, $randInt);
            }
            for ($k = 0; $k < self::$elementOutputCount; $k++) {
                $schema[$i]["OUTPUT"][] = "Out".$outCounter;
                $allInput[] = "Out".$outCounter;
                $outCounter++;
            }
        }
        $this->setSchema($schema);
        $this->correction();
       
        return $this;
    }

    public function correction()
    {
        $freeInputMarks = $this->getFreeInputMarks();
        foreach ($freeInputMarks as $inputMark) {
            foreach ($this->schema as $obj) {
                foreach ($obj->getInputMarks() as $key => $value) {
                    if (in_array($value, self::$staticInputMarks)) {
                        $obj->setInputMark($inputMark, $key);
                        break(2);
                    }
                }
            }
        }
      
        return $this;
    }

    public function crossover(Schema $accepter)
    {
        $accepterSchema = $accepter->getSchema();
        $randInt = rand(0, sizeof($this->schema)-1);
        $accepterElement = clone $accepterSchema[$randInt];
        $this->schema[$randInt] = $accepterElement;
      
        return $this;
    }

    public function mutation()
    {
        $randInt = rand(0, sizeof($this->schema)-1);
        $randElement = $this->schema[$randInt];

        $freeInput = array();
        $diff = array_diff($this->getFreeInputs(), $this->getSkipInputsToElement($randInt));
        foreach ($diff as $value) {
            $freeInput[] = $value;
        }
        if (!empty($freeInput)) {
            $randI = rand(0, sizeof($freeInput)-1);
            //var_dump(2222222222,$randInt, $randI, $freeInput, $freeInput[$randI], $this->schema);
            if (!in_array($freeInput[$randI], $randElement->getOutputMarks())) {
                $randElement->setRandomInputMark($freeInput[$randI]);
            }
        }
        //var_dump(333333,$this->schema);
      
        return $this;
    }

    protected function run()
    {
        $results = array();
        foreach (self::$outputMarks as $outputMark) {
            foreach ($this->getFreeOutputs() as $output) {
                //var_dump($outputMark, $output);
                $tmpResults = array();
                foreach (Main::getInstance()->getInput() as $allInput) {
                    //var_dump($allInput);
                    foreach ($this->schema as $obj) {
                        $allInput = array_merge($allInput, $obj->run($allInput)->getOutput());
                    }
                    $tmpResults[] = $allInput[$outputMark] == $allInput[$output] ? 1 : 0;
                }
                $results[$output] = array_sum($tmpResults) / count($tmpResults);
                //var_dump($tmpResults);
            }
            //var_dump($results, $outputMark);
        }

        //var_dump($results, $outputMark);
        //$finalOutput = end($this->getFreeOutputs());
        //var_dump($this->getFreeOutputs());

        /*$allOutput = Main::getInstance()->getOutput();
        var_dump($allOutput);*/

        /*foreach (Main::getInstance()->getInput() as $allInput) {
            foreach ($this->schema as $obj) {
                $allInput = array_merge($allInput, $obj->run($allInput)->getOutput());
            }
            $results[] = $allInput["Out"] == $allInput[$finalOutput] ? 1 : 0;
        }
*/
        return $results;
    }

    public function getEstimate()
    {
        $results = $this->run();

        return max($results);
    }
}