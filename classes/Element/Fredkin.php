<?
namespace Application\Element;

use Application\Interfaces\IElement;

class Fredkin implements IElement
{
    protected $inputMarks = array();
    protected $outputMarks = array();
    protected $output = array();
    protected static $inputCount = 3;
    protected static $outputCount = 3;

    public static function getInputCount()
    {
        return self::$inputCount;
    }

    public static function getOutputCount()
    {
        return self::$outputCount;
    }

    public function setInputMark($inputMark, $position = 0)
    {
        $this->inputMarks[$position] = $inputMark;

        return $this;
    }

    public function setRandomInputMark($inputMark)
    {
        $randInt = rand(0, self::$inputCount-1);
        $this->inputMarks[$randInt] = $inputMark;

        return $this;
    }

    public function setInputMarks($inputMarks)
    {
        $this->inputMarks = $inputMarks;

        return $this;
    }

    public function getInputMarks()
    {
        return $this->inputMarks;
    }

    public function setOutputMarks($outputMarks)
    {
        $this->outputMarks = $outputMarks;

        return $this;
    }

    public function getOutputMarks()
    {
        return $this->outputMarks;
    }

    public function run($input)
    {
        $this->output = array();

        $this->output[$this->outputMarks[0]] = $input[$this->inputMarks[0]];
        if (
            (!$input[$this->inputMarks[0]] && $input[$this->inputMarks[1]]) 
            || 
            ($input[$this->inputMarks[0]] && $input[$this->inputMarks[2]])
        ) {
            $this->output[$this->outputMarks[1]] = 1;
        } else {
            $this->output[$this->outputMarks[1]] = 0;
        }
        if (
            ($input[$this->inputMarks[0]] && $input[$this->inputMarks[1]]) 
            || 
            (!$input[$this->inputMarks[0]] && $input[$this->inputMarks[2]])
        ) {
            $this->output[$this->outputMarks[2]] = 1;
        } else {
            $this->output[$this->outputMarks[2]] = 0;
        }

        return $this;
    }

    public function getOutput()
    {
        return $this->output;
    }
}