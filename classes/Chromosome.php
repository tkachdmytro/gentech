<?
namespace Application;

use Application\Main;
use Application\Schema;

class Chromosome
{
    protected $schema;

    public function __clone()
    {
        $this->schema = clone $this->schema;
    }

    public static function create()
    {
        return new static();
    }

    public function setSchema(Schema $schema)
    {
        $this->schema = $schema;

        return $this;
    }

    public function getSchema()
    {
        return $this->schema;
    }

    public function getCode()
    {
        return $this->schema->getSchemaCode();
    }

    public function getEstimate()
    {
        return $this->schema->getEstimate();
    }

    public function getStringCode($separator = "|")
    {
        return implode($separator, $this->code);
    }

    public function getClone()
    {
        return clone $this;
    }

    public function crossover(Chromosome $accepter)
    {
        $newChromosome = $this->getClone();
        $newChromosome->schema->crossover($accepter->getSchema());

        return $newChromosome;
    }

    public function mutation()
    {
        $this->schema->mutation();
        
        return $this;
    }

    public function generate()
    {
        $this->schema = Schema::create()->generate();

        return $this;
    }
}