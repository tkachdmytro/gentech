<?
namespace Application;

use Application\Main;
use Application\ElementFactory;

class SchemaBuilder
{
    protected static $instance;
    protected $elementsCount;
    protected $elementsType;
    protected $elementInputCount;
    protected $elementOutputCount;
    protected $elementsInputCount;
    protected $elementsOutputCount;
    protected $inputMarks = array();
    protected $outputMarks = array();
    protected $staticInputMarks = array();
    protected $allMarks = array();
    protected $allInputMarks = array();
    protected $skipInputMarks = array();
    protected $allOutputMarks = array();

    public static function getInstance()
    {
        if (!isset(static::$instance))
        {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function setElementsCount($elementsCount)
    {
        $this->elementsCount = $elementsCount;

        return $this;
    }

    public function getElementsCount()
    {
        return $this->elementsCount;
    }

    public function setElementsType($elementsType)
    {
        $this->elementsType = $elementsType;

        return $this;
    }

    public function getElementsType()
    {
        return $this->elementsType;
    }

    public function setInputMarks($inputMarks)
    {
        $this->inputMarks = $inputMarks;

        return $this;
    }

    public function getInputMarks()
    {
        return $this->inputMarks;
    }

    public function setOutputMarks($outputMarks)
    {
        $this->outputMarks = $outputMarks;

        return $this;
    }

    public function getOutputMarks()
    {
        return $this->outputMarks;
    }

    public function getAllInputMarks()
    {
        return $this->allInputMarks;
    }

    public function getAllOutputMarks()
    {
        return $this->allOutputMarks;
    }

    public function getAllMarks()
    {
        return $this->allMarks;
    }

    public function getSkipInputMarks()
    {
        return $this->skipInputMarks;
    }

    public function getElementInputCount()
    {
        return $this->elementInputCount;
    }

    public function getElementOutputCount()
    {
        return $this->elementOutputCount;
    }

    public function getElementsInputCount()
    {
        return $this->elementsInputCount;
    }

    public function getElementsOutputCount()
    {
        return $this->elementsOutputCount;
    }

    public function getStaticInputMarks()
    {
        return $this->staticInputMarks;
    }

    public function build()
    {
        $this->elementInputCount = ElementFactory::getInputCount($this->getElementsType());
        $this->elementOutputCount = ElementFactory::getOutputCount($this->getElementsType());
        $this->elementsInputCount = ElementFactory::getInputCount($this->getElementsType()) * $this->getElementsCount();
        $this->elementsOutputCount = ElementFactory::getOutputCount($this->getElementsType()) * $this->getElementsCount();
        //generate static marks
        for ($i = 1; $i <= $this->elementsInputCount; $i++) { 
            $this->staticInputMarks[] = strval(rand(0, 1000) % 2);
        }
        $this->allInputMarks = array_merge($this->inputMarks,  $this->staticInputMarks);
        for ($k = 1; $k <= $this->elementsOutputCount; $k++) {
            $this->allInputMarks[] = "Out".$k;
            $this->allOutputMarks[] = "Out".$k;
        }
        /*$withoutLastElement = $this->elementsOutputCount - $this->getElementsCount();
        for ($k = 1; $k <= $withoutLastElement; $k++) {
            $this->allInputMarks[] = "Out".$k;
        }*/
        $this->allMarks[] = array_merge($this->allInputMarks,  $this->allOutputMarks);


        for ($i = 0; $i < $this->getElementsCount(); $i++) {
            if ($i > 0) {
                $this->skipInputMarks[] =  array_slice($this->allOutputMarks, $i * $this->elementOutputCount);
            } else {
                $this->skipInputMarks[] = $this->allOutputMarks;
            }
        }

        return $this;
    }
}