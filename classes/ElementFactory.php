<?
namespace Application;

use Application\Element\I;
use Application\Element\Fredkin;

class ElementFactory
{
    public static function create($type = "I")
    {
        switch ($type) {
            case "I":
                return new I();
                break;

            case "Fredkin":
                return new Fredkin();
                break;
            
            default:
                return false;
                break;
        }
    }

    public static function getInputCount($type = "I")
    {
        switch ($type) {
            case "I":
                return I::getInputCount();
                break;

            case "Fredkin":
                return Fredkin::getInputCount();
                break;
            
            default:
                return false;
                break;
        }
    }

    public static function getOutputCount($type = "I")
    {
        switch ($type) {
            case "I":
                return I::getOutputCount();
                break;

            case "Fredkin":
                return Fredkin::getOutputCount();
                break;
            
            default:
                return false;
                break;
        }
    }
}
