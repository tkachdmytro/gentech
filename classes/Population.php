<?
namespace Application;

use Application\AbstractPopulation;
use Application\Chromosome;

class Population extends AbstractPopulation
{
    protected static $instance;
    protected $populations = array();
    protected $maxPopulation = 10;

    public static function getInstance()
    {
        if (!isset(static::$instance))
        {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function allMutation()
    {
        $newPolulations = array();
        /*$randInt = rand(0, $this->maxPopulation);
        for ($i=0; $i < $randInt; $i++) { 
            
        }*/
        $newPolulations[] = $this->getBestIndividual();
        foreach ($this->populations as $individual) {
            $newPolulations[] = $individual
                ->crossover($this->findCouple($individual))
                ->mutation();
        }

        $this->populations = $newPolulations;

        return $this;
    }

    public function generateBase()
    {
        for ($i = 0; $i < $this->getMaxPopulation(); $i++) {
            $this->addIndividuals(Chromosome::create()->generate());
        }

        return $this;
    }

    public function generate()
    {
        $newPolulations = array();
        foreach ($this->populations as $individual) {
            $newPolulations[] = $individual;
            $newPolulations[] = $individual
                ->crossover($this->findCouple($individual))
                ->mutation();
        }
        $this->populations = $newPolulations;

        return $this;
    }

    public function selection()
    {
        $this->sort();

        $offset = intval($this->getMaxPopulation() / 2);
        if ($offset > 0)
        {
            $this->populations = array_slice($this->populations, 0, $offset, true);
        } 

        return $this;
    }
}