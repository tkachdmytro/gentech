<?
namespace Application;

use Application\ReaderCSV;
use Application\Population;
use Application\Chromosome;
use Application\Schema;
use Application\SchemaBuilder;

class Main
{
    const FINAL_ESTIMATE = 1;
    const MAX_REPEAT_ESTIMATE = 5;

    protected static $instance;
    protected static $objSchemaBuilder;
    protected $counter = 0;
    protected $counterWithoutChanges = 0;
    protected $filename;
    protected $elementsType;
    protected $input = array();
    protected $output = array();
    protected $inputMarks = array();
    protected $outputMarks = array();
    protected $objPopulation;
    protected $bestEstimate;
    protected $maxPopulation = 10;
    protected $maxEvolution = 100;
    protected $elementsCount = 3;

    public function __construct () {
        $this->objPopulation = Population::getInstance();
    }

    public static function getInstance()
    {
        if (!isset(static::$instance))
        {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function setElementsType($elementsType)
    {
        $this->elementsType = $elementsType;

        return $this;
    }

    public function getElementsType()
    {
        return $this->elementsType;
    }

    public function setInput($input)
    {
        $this->input = $input;

        return $this;
    }

    public function addInput($input)
    {
        $input["0"] = 0;
        $input["1"] = 1;
        $this->input[] = $input;

        return $this;
    }

    public function getInput()
    {
        return $this->input;
    }

    public function setInputMarks($inputMarks)
    {
        $this->inputMarks = $inputMarks;

        return $this;
    }

    public function getInputMarks()
    {
        return $this->inputMarks;
    }

    public function setOutputMarks($outputMarks)
    {
        $this->outputMarks = $outputMarks;

        return $this;
    }

    public function getOutputMarks()
    {
        return $this->outputMarks;
    }

    public function setOutput($output)
    {
        $this->output = $output;

        return $this;
    }

    public function getOutput()
    {
        return $this->output;
    }

    public function setMaxPopulation($maxPopulation)
    {
        $this->maxPopulation = $maxPopulation;

        return $this;
    }

    public function getMaxPopulation()
    {
        return $this->maxPopulation;
    }

    public function setMaxEvolution($maxEvolution)
    {
        $this->maxEvolution = $maxEvolution;

        return $this;
    }

    public function getMaxEvolution()
    {
        return $this->maxEvolution;
    }

    public function setElementsCount($elementsCount)
    {
        $this->elementsCount = $elementsCount;

        return $this;
    }

    public function getElementsCount()
    {
        return $this->elementsCount;
    }

    public function getSchemaBuilder()
    {
        return self::$objSchemaBuilder;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename . '.csv';

        return $this;
    }

    public function changePopulation() {
        $this->objPopulation->allMutation();

        return true;
    }

    public function start()
    {
        $this->counter = 0;
        $csv = new ReaderCSV();
        $csv->readCSV($_SERVER["DOCUMENT_ROOT"] . "/csv/" . $this->filename, true);

        $this->input = array();
        $this->output = array();
        $this->setInputMarks($csv->getInputMarks());
        $this->setOutputMarks($csv->getOutputMarks());

        for ($i = 0; $i < $csv->totalRows(); $i++) {
            $this->addInput($csv->getRow($i));
        }

        self::$objSchemaBuilder = SchemaBuilder::getInstance()
            ->setElementsType($this->getElementsType())
            ->setElementsCount($this->getElementsCount())
            ->setInputMarks($this->getInputMarks())
            ->setOutputMarks($this->getOutputMarks())
            ->build();

        //step 1 - Base Population generator
        $this->objPopulation->setMaxPopulation($this->getMaxPopulation())->generateBase();
        $this->bestEstimate = $this->objPopulation->getBestIndividual()->getEstimate();
        
        //var_dump($objPopulation->getPopulations());
        //$arPopulation = $objPopulation->getInfo();
        //$arPopulation = $this->objPopulation->getEstimates();
        //var_dump($objPopulation->getBestIndividual());
        //var_dump($objPopulation->getInfo());
        //die();

        return $this;
    }

    //next step
    public function next()
    {
        $this->counter++;
        $this->objPopulation->selection()->generate();
        $currentEstimate = $this->objPopulation->getBestIndividual()->getEstimate();
        if ($this->bestEstimate == $currentEstimate) {
            $this->counterWithoutChanges++;
        } else {
            $this->bestEstimate = $currentEstimate;
        }

        if ($this->counterWithoutChanges == self::MAX_REPEAT_ESTIMATE) {
            $this->counterWithoutChanges = 0;
            $this->changePopulation();
        }

        if ($this->bestEstimate == self::FINAL_ESTIMATE) {
            $this->finish();
            return true;
        }
        var_dump($this->counter . " ---------------------- \n", $this->bestEstimate);


        return false;
    }

    //finish step
    public function finish()
    {
        $message = "Step - " . $this->counter;
        $message .= "\n----------------------\n";
        $message .= "Schema finded \n";
        $message .= "Estimate - " . $this->bestEstimate;

        $shemaJson = $this->objPopulation->getBestIndividual()->getSchema()->getSchemaJSON();

        var_dump($message , $this->objPopulation->getBestIndividual());
        //var_dump($this->objPopulation->getInfo());
        var_dump($shemaJson);

        return true;
    }

    public static function array_skip($array, $position)
    {
        $result = array();
        foreach ($array as $key => $value) {
            if($key == $position) continue;
            $result[] = $value;
        }

        return $result;
    }
}