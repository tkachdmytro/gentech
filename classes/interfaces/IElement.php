<?
namespace Application\Interfaces;

interface IElement
{
    public static function getInputCount();
    public static function getOutputCount();
    public function setInputMarks($inputMarks);
    public function setRandomInputMark($inputMark);
    public function getInputMarks();
    public function setOutputMarks($outputMarks);
    public function getOutputMarks();
    public function run($input);
    public function getOutput();
}