<?
namespace Application\Interfaces;

interface IReader
{
    public function setFilename($filename);
    public function getFilename();
    public function setInputMarks($inputMarks);
    public function getInputMarks();
    public function setOutputMarks($outputMarks);
    public function getOutputMarks();

    //public function getOutput();
}